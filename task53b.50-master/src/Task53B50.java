import s50.*;
public class Task53B50 {
    public static void main(String[] args) throws Exception {
        //task 4
        Customer customer1 = new Customer(1, "Huy", 20);
        Customer customer2 = new Customer(2, "Yen", 10);
        System.out.println(customer1);
        System.out.println(customer2);
        //task 5
        Account account1 = new Account(1001, customer1, 10000000);
        Account account2 = new Account(1002, customer2, 20000000);
        System.out.println(account1);
        System.out.println(account2);
    }
}
